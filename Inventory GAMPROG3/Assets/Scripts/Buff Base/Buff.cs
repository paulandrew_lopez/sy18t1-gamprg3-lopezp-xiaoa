﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Buff : MonoBehaviour {
    public enum StackType
    {
        Intensity = 0,
        Duration = 1,
        NotStackable = 2
    }

    public StackType _StackType = 0;
    public string BuffName = "";
    public float BuffDuration = 0.0f;
    public float BuffMaxDuration = 0.0f;
    public string BuffDescription = "";
    public bool EnableBuffDurationCountdown = false;
    public GameObject Receiver;

    protected virtual void Start()
    {
        Activate();
    }
    protected virtual void Update()
    {
        TickCountdownUpdate();
    }
    private void TickCountdownUpdate()
    {
        if (!EnableBuffDurationCountdown || BuffDuration == Mathf.Infinity) return;

        if (BuffMaxDuration < BuffDuration) BuffMaxDuration = BuffDuration;

        BuffDuration -= Time.deltaTime;
        if (BuffDuration < 0.0f)
            Destroy(this);
    }
    /// <summary>
    /// Buff Activate
    /// </summary>
    protected abstract void Activate();
    /// <summary>
    /// Buff Deactive
    /// </summary>
    protected abstract void Deactive();
    public abstract void AddStack();
    public abstract void RemoveStack();
    protected virtual void OnDestroy()
    {
        Deactive();
    }
}
