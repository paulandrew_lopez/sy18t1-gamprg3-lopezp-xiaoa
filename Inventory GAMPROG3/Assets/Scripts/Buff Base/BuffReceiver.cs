﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiver : MonoBehaviour {

    public List<GameObject> Buffs;

    public void Receive(GameObject obj)
    {
        GameObject buffObj = GetBuffObj(obj);

        //Check if obj exist
        if(buffObj)
        {
            buffObj.GetComponent<Buff>().AddStack();
        }
        else
        {
            GameObject newObj = Instantiate(obj);
            newObj.GetComponent<Buff>().Receiver = this.gameObject;

            Buffs.Add(newObj);
        }
    }

    private GameObject GetBuffObj(GameObject obj)
    {
        if (Buffs.Count == 0) return null;

        foreach (var buff in Buffs)
        {
            if (buff.GetComponent<Buff>().BuffName == obj.GetComponent<Buff>().BuffName)
            {
                //if Exist
                return buff;
            }
        }

        //If not
        return null;
    }

    public GameObject GetBuffObjWithBuff(Buff buff)
    {
        if (Buffs.Count == 0) return null;

        foreach (var item in Buffs)
        {
            if (item.GetComponent<Buff>().BuffName == buff.BuffName)
            {
                return item;
            }
        }

        return null;
    }

    public void RemoveSpecificBuff(Buff buff)
    {
        if (Buffs.Count == 0) return;

        if(GetBuffObjWithBuff(buff))
        {
            Destroy(GetBuffObjWithBuff(buff));
        }
    }
}
