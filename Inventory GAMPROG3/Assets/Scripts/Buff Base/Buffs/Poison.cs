﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : Buff
{
    public int MaxPoisonIntesity = 5;
    public int CurPoisonIntensity = 1;

    public float PoisonStackDamage = 0.02f;  

    private Health PlayerHealth;
    private float CurHp;
    private float DamagePerSec;

    protected override void Start()
    {
        PlayerHealth = Receiver.GetComponent<Health>();
        base.Start();
    }

    protected override void Activate()
    {
        //Sets damage upon start
        DamagePerSec = (PlayerHealth.MaxHP * PoisonStackDamage) * CurPoisonIntensity;

        StartCoroutine(Poisoned());
    }

    protected override void Deactive()
    {
        throw new NotImplementedException();
    }

    public override void AddStack()
    {
        //Returns if the intensity exceeds max
        if (CurPoisonIntensity >= MaxPoisonIntesity) return;
        //updating the damage based on the intensity
        UpdateIntensity();
        
    }

    IEnumerator Poisoned()
    {
        while (BuffDuration > 0.0f)
        {
            if (PlayerHealth.HP > 0)
            {
                //Deducts the health based on the damage
                PlayerHealth.HP -= DamagePerSec;
                yield return new WaitForSeconds(1.0f);
            }
            else
            {
                //Exits when HP < 0
                yield return null;               
            } 
        }
    }

    public override void RemoveStack()
    {
        throw new NotImplementedException();
    }

    protected override void OnDestroy()
    {
        BuffReceiver buffReceiver = Receiver.GetComponent<BuffReceiver>();
        buffReceiver.Buffs.Remove(buffReceiver.GetBuffObjWithBuff(GetComponent<Poison>()));
        base.OnDestroy();
    }

    void UpdateIntensity()
    {
        CurPoisonIntensity++;
        DamagePerSec = (PlayerHealth.MaxHP * PoisonStackDamage) * CurPoisonIntensity;
    }
}
