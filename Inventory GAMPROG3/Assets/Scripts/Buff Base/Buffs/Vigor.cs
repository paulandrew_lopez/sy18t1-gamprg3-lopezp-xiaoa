﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vigor : Buff
{
    public float VitMultiplier = 1.5f;
    public float VigorMaxDuration = 15f;
    private Stats PlayerStats;
    private float TempVit;
    private float OriginalVit;

    protected override void Start()
    {
        PlayerStats = Receiver.GetComponent<Stats>();
        base.Start();
    }

    protected override void Activate()
    {
        //Stores the Original vit
        OriginalVit = PlayerStats.Vitality;
        TempVit = OriginalVit;
        //Multiplying the current vit with the multiplier
        TempVit *= VitMultiplier;
        PlayerStats.Vitality = TempVit;
    }

    protected override void Deactive()
    {
        //Sets vit to original value
        PlayerStats.Vitality = OriginalVit;
    }

    public override void AddStack()
    {
        BuffDuration += BuffMaxDuration;
        BuffMaxDuration += VigorMaxDuration;
    }

    public override void RemoveStack()
    {
        throw new NotImplementedException();
    }

    protected override void OnDestroy()
    {
        BuffReceiver buffReceiver = Receiver.GetComponent<BuffReceiver>();
        buffReceiver.Buffs.Remove(buffReceiver.GetBuffObjWithBuff(GetComponent<Vigor>()));
        base.OnDestroy();
    }
}
