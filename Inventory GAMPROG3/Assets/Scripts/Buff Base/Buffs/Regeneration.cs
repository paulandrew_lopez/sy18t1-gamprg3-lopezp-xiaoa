﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regeneration : Buff
{
    public float RegenPercentage = 0.05f;
    public float RegenMaxDur = 3f;
    private Health PlayerHealth;
    private float CurHP;
    private float HealAmount;

    protected override void Start()
    {
        PlayerHealth = Receiver.GetComponent<Health>();
        base.Start();
    }

    protected override void Activate()
    {
        //sets heal amount per second
        HealAmount = PlayerHealth.MaxHP * RegenPercentage;
        StartCoroutine(Regen());
    }

    protected override void Deactive()
    {
        throw new NotImplementedException();
    }

    public override void AddStack()
    {
        BuffDuration += BuffMaxDuration;
        BuffMaxDuration += RegenMaxDur;
    }

    IEnumerator Regen()
    {
        while (BuffDuration > 0.0f)
        {
            if (PlayerHealth.HP < PlayerHealth.MaxHP)
            {
                //Heal based on the the heal amount
                PlayerHealth.HP += HealAmount;
                yield return new WaitForSeconds(1.0f);
            }
            else
            {
                //Exits if HP is greater than max HP
                yield return null;
            }
        }
    }

    public override void RemoveStack()
    {
        throw new NotImplementedException();
    }

    protected override void OnDestroy()
    {
        BuffReceiver buffReceiver = Receiver.GetComponent<BuffReceiver>();
        buffReceiver.Buffs.Remove(buffReceiver.GetBuffObjWithBuff(GetComponent<Regeneration>()));
        base.OnDestroy();
    }
}
