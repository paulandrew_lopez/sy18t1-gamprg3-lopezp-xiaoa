﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Might : Buff
{
    public int MaxMightIntensity = 10;
    public int CurMightIntensity = 1;
    public float PowPerStack = 100;

    private Stats PlayerStats;
    private float OriginalPow;
    private float TempPow;


    protected override void Start()
    {
        PlayerStats = Receiver.GetComponent<Stats>();
        base.Start();
    }

    protected override void Activate()
    {
        //Stores the original Power
        OriginalPow = PlayerStats.GetComponent<Stats>().Power;
        UpdateIntensity();
    }

    protected override void Deactive()
    {
        //Sets Original Power
        PlayerStats.Power = OriginalPow;
    }

    public override void AddStack()
    {
        //Checks if intensity exceeds max
        if (CurMightIntensity >= MaxMightIntensity) return;
        CurMightIntensity++;
        UpdateIntensity();
    }
    public override void RemoveStack()
    {
        CurMightIntensity--;
        UpdateIntensity();
        if (CurMightIntensity < 0) Destroy(this);
    }
    protected override void OnDestroy()
    {
        BuffReceiver buffReceiver = Receiver.GetComponent<BuffReceiver>();
        buffReceiver.Buffs.Remove(buffReceiver.GetBuffObjWithBuff(GetComponent<Might>()));
        base.OnDestroy();
    }

    void UpdateIntensity()
    {
        TempPow = OriginalPow + (CurMightIntensity * PowPerStack);
        //Sets the Power to Temp
        PlayerStats.Power = TempPow;
    }
}
