﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

    const float PowerInit = 10.0f;
    const float PrecisionInit = 5.0f;
    const float ToughnessInit = 5.0f;
    const float VitalityInit = 5.0f;

    public float Power = PowerInit;
    public float Precision = PrecisionInit;
    public float Toughness = ToughnessInit;
    public float Vitality = VitalityInit;
}
