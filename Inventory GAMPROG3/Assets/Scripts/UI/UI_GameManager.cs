﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GameManager : MonoBehaviour {

    public Health health;

    [Header("UI")]
    public GameObject GameOverPanel;

    private bool isDead = false;

	// Update is called once per frame
	void Update () {
        //When is Dead, return
        if (isDead) return;

        //Check player is Alive
		if(health.IsAlive())
        {
            //Set to dead
            isDead = true;

            //Do Game Over behavior
            OnGameOver();
        }
	}

    void OnGameOver()
    {
        //Stop the Time
        Time.timeScale = 0.0f;

        //Show the GameOverPanel
        GameOverPanel.SetActive(true);
    }

    public void OnClickExit()
    {
        //Quit the Game
        //Only works in Builds
        Application.Quit();
    }
}
