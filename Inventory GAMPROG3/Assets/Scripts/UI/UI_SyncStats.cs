﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SyncStats : MonoBehaviour {

    const string ENDLINE = "\n";
    public Health _Health;
    public Stats _Stats;

    public Text StatValueText;

	// Update is called once per frame
	void Update () {
        SyncStats();
	}

    void SyncStats()
    {
        //Clear Text
        StatValueText.text = "";

        //Health
        StatValueText.text += _Health.HP.ToString() + ENDLINE + ENDLINE;

        //Power
        StatValueText.text += _Stats.Power + ENDLINE;

        //Precision
        StatValueText.text += _Stats.Precision + ENDLINE;

        //Toughness
        StatValueText.text += _Stats.Toughness + ENDLINE;

        //Vitality
        StatValueText.text += _Stats.Vitality;
    }
}
