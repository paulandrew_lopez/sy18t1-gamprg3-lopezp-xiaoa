﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//To be Place in the Parent Gameobject of Inventory Images to be displayed
public class UI_SyncInventory : MonoBehaviour {

    public Inventory MainInventory;
    private List<GameObject> childTransforms = new List<GameObject>();

	// Use this for initialization
	void Start () {
        GetChildImageTransforms();
    }
	
	// Update is called once per frame
	void Update () {
        SyncInventory();
	}

    void SyncInventory()
    {
        //Clear Images UI
        ClearImageTransformsData();

        //Get Reference of Inventory Items
        List<ItemBase> InventoryList = MainInventory.InventoryList;

        //Show the Inventory Item in UI Images
        for (int i = 0; i < InventoryList.Count; i++)
        {
            if (InventoryList[i])
            {
                childTransforms[i].GetComponent<UI_Item>().Item = InventoryList[i];
                childTransforms[i].GetComponent<UI_ItemBehavior>().Item = InventoryList[i];
            }
            else
            {
                childTransforms[i].GetComponent<UI_Item>().Item = null;
                childTransforms[i].GetComponent<UI_ItemBehavior>().Item = null;
            }
        }
    }
    /// <summary>
    /// Get All Child Transforms of this gameobject
    /// </summary>
    void GetChildImageTransforms()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            childTransforms.Add(transform.GetChild(i).gameObject);
        }
    }
    /// <summary>
    /// Reset UI Images in the inventory
    /// </summary>
    void ClearImageTransformsData()
    {
        foreach (var child in childTransforms)
        {
            child.GetComponent<UI_Item>().Item = null;
            child.GetComponent<UI_ItemBehavior>().Item = null;
        }
    }
}
