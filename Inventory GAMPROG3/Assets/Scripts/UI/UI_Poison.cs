﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UI_Poison : MonoBehaviour
{
    public Image PoisonImage;
    public Text PoisonText;
    public BuffReceiver _BuffReceiver;
    public GameObject PoisonPrefab;


    private GameObject poisonBuff;
    private Poison poison;

    void Update()
    {

        poisonBuff = _BuffReceiver.GetBuffObjWithBuff(PoisonPrefab.GetComponent<Poison>());
        if (poisonBuff)
        {
            poison = poisonBuff.GetComponent<Poison>();
        }

        //Check if the player has the Poison debuff
        if (poisonBuff)
        {
            PoisonText.text = poison.CurPoisonIntensity.ToString();
            PoisonImage.fillAmount = poison.BuffDuration / poison.BuffMaxDuration;
        }
        else
        {

            //Resets the image and text when Poison runs out
            PoisonImage.fillAmount = 1;
            PoisonText.text = "0";
            return;
        }
    }
}
