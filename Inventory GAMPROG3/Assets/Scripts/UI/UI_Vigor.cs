﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;

public class UI_Vigor : MonoBehaviour
{
    public Image VigorImage;
    public BuffReceiver _BuffReceiver;
    public GameObject VigorPrefab;

    private GameObject vigorBuff;
    private Vigor vigor;

    void Update()
    {

        vigorBuff = _BuffReceiver.GetBuffObjWithBuff(VigorPrefab.GetComponent<Vigor>());
        if (vigorBuff)
        {
            vigor = vigorBuff.GetComponent<Vigor>();
        }

        //Checks if the player has the Vigor buff
        if (vigorBuff)
        {
            VigorImage.fillAmount = vigor.BuffDuration / vigor.BuffMaxDuration;
        }
        else
        {
            //Resets the image when Vigor runs out
            VigorImage.fillAmount = 1;
        }
    }
}
