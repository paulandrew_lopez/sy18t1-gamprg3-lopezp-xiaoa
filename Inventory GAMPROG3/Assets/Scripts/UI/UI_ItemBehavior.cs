﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UI_Item))]
public class UI_ItemBehavior : MonoBehaviour {

    public ItemBase Item;
    private Button button;
    private UI_Item ui_Item;
    private ItemBase prevItem;

	// Use this for initialization
	void Start () {
        ui_Item = GetComponent<UI_Item>();
        button = GetComponent<Button>();

        prevItem = Item;
	}
	
	// Update is called once per frame
	void Update () {
        //Store prev Item
        if (prevItem == null && Item)
        {
            prevItem = Item;
        }

        OnPrevItemChanged();

        //If Item exist, button become interactable else button is not interactable
        if(Item)
        {
            EnableButtonComponent();
        }
        else
            DisableButtonComponent();
    }
    /// <summary>
    /// Do Enable button and other related behavior
    /// </summary>
    void EnableButtonComponent()
    {
        //If button is not interactable
        if (!button.enabled)
        {
            //Set button to interactable
            button.enabled = true;

            //AddListener when the Item is clicked
            if (Item.ItemType == ItemBase.TypeOfItem.Both || Item.ItemType == ItemBase.TypeOfItem.Usable)
            {
                button.onClick.AddListener(Item.UseItem);
            }
            else
            {
                button.onClick.AddListener(Item.DropItem);
            }
            button.onClick.AddListener(ui_Item.OnItemUsed);
            button.onClick.AddListener(OnItemUsed);
            button.onClick.AddListener(DisableButtonComponent);
        }
    }
    /// <summary>
    /// Do Disable button and other related behavior
    /// </summary>
    void DisableButtonComponent()
    {
        //Renew Unity Event
        button.onClick = new Button.ButtonClickedEvent();

        //If button is interactable
        if (button.enabled)
        {
            //Set button to not interactable
            button.enabled = false;
        }
    }
    /// <summary>
    /// Call to make Item null
    /// </summary>
    public void OnItemUsed()
    {
        Item = null;
    }
    /// <summary>
    /// Call when Previous Item is not the current Item
    /// </summary>
    void OnPrevItemChanged()
    {
        if (Item && prevItem)
        {
            if (prevItem != Item)
            {
                prevItem = Item;
                DisableButtonComponent();
            }
        }
    }
}
