﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UI_Regeneration : MonoBehaviour
{
    public Image RegenerationImage;
    public BuffReceiver _BuffReceiver;
    public GameObject RegenPrefab;

    private GameObject regenBuff;
    private Regeneration regen;

    void Update()
    {

        regenBuff = _BuffReceiver.GetBuffObjWithBuff(RegenPrefab.GetComponent<Regeneration>());
        if (regenBuff)
        {
            regen = regenBuff.GetComponent<Regeneration>();
        }

        //Checks if the player has the Regeneration buff
        if (regenBuff)
        {
            RegenerationImage.fillAmount = regen.BuffDuration / regen.BuffMaxDuration;
        }
        else
        {
            //Resets image when Regeneration runs out
            RegenerationImage.fillAmount = 1;
            return;
        }
    }
}
