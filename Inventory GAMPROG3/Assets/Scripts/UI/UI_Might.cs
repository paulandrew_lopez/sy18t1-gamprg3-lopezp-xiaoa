﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UI_Might : MonoBehaviour
{
    public Text MightText;
    public BuffReceiver _BuffReceiver;
    public GameObject MightPrefab;

    private GameObject mightBuff;
    private Might might;

    void Update()
    {
        mightBuff = _BuffReceiver.GetBuffObjWithBuff(MightPrefab.GetComponent<Might>());
        if (mightBuff)
        {
            might = mightBuff.GetComponent<Might>();
        }

        //Checks if the player has the Might buff
        if (mightBuff)
        {
            MightText.text = might.CurMightIntensity.ToString();
        }
        else
        {
            //Resets the text when Might is gone
            MightText.text = "0";
            return;
        }
    }
}
