﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Item : MonoBehaviour {

    public ItemBase Item;
    public int ItemName_ChildIndex = 0;
    public int ItemStackNum_ChildIndex = 1;

    public enum DisplayState
    {
        ItemName,
        All
    }
    public DisplayState _DisplayState = DisplayState.ItemName;

    private Text displayItemText;
    private Text displayItemStackText;

    private void Start()
    {
        //GetTextComponent for ItemStackNumber display
        if (_DisplayState == DisplayState.All)
            displayItemStackText = transform.GetChild(ItemStackNum_ChildIndex).GetComponent<Text>();

        //GetTextComponent for ItemName display
        displayItemText = transform.GetChild(ItemName_ChildIndex).GetComponent<Text>();
    }

    private void Update()
    {
        //No Item
        if (!Item)
        {
            SetBlankText();
            return;
        }
        //There is an Item
        else
        {
            //Display Item Stack Number
            if(_DisplayState == DisplayState.All)
                displayItemStackText.text = Item.CurStacks.ToString();

            //Display Item Name
            displayItemText.text = Item.Name;
        }
    }

    void SetBlankText()
    {
        if (_DisplayState == DisplayState.All)
            if (displayItemStackText.text != "") displayItemStackText.text = "";
        if (displayItemText.text != "") displayItemText.text = "";
    }
    /// <summary>
    /// Call to make Item null
    /// </summary>
    public void OnItemUsed()
    {
        Item = null; 
    }
}
