﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antidote : ItemBase
{
    public GameObject Player;
    public int MaxItemStack = 10;
    public int MinItemStack = 1;
    public string ItemDescription = "Cures Poison status (clears all stacks)";

    [Header("Buff")]
    public GameObject PoisonBuff;

    private BuffReceiver buffReceiver;

    private void Start()
    {
        buffReceiver = Player.GetComponent<BuffReceiver>();

        Name = "Antidote";
        CurStacks = MinItemStack;
        MaxStacks = MaxItemStack;
        ItemType = TypeOfItem.Both;
        Description = ItemDescription;
    }

    public override void UseItem()
    {
        buffReceiver.RemoveSpecificBuff(PoisonBuff.GetComponent<Buff>());
        //Call this to remove the item used
        base.UseItem();
    }
}
