﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RejuvenatingPotion : ItemBase
{
    public GameObject Player;
    public int MaxItemStack = 10;
    public int MinItemStack = 1;
    public string ItemDescription = "Applies Vigor for 15s";

    [Header("Buff")]
    public GameObject VigorBuff;

    private BuffReceiver buffReceiver;

    private void Start()
    {
        buffReceiver = Player.GetComponent<BuffReceiver>();

        Name = "Rejuvenating Potion";
        CurStacks = MinItemStack;
        MaxStacks = MaxItemStack;
        ItemType = TypeOfItem.Both;
        Description = ItemDescription;
    }

    public override void UseItem()
    {
        buffReceiver.Receive(VigorBuff);
        base.UseItem();
    }
}
