﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorsEmblem : ItemBase {

    public GameObject Player;
    public int MaxItemStack = 10;
    public int MinItemStack = 1;
    public string ItemDescription = "Gain 1 stack of Might as long as it’s in your inventory. Does not expire.";

    [Header("Buff")]
    public GameObject MightBuff;

    private BuffReceiver buffReceiver;

    private void Start()
    {
        buffReceiver = Player.GetComponent<BuffReceiver>();

        Name = "Warrior's Emblem";
        CurStacks = MinItemStack;
        MaxStacks = MaxItemStack;
        ItemType = TypeOfItem.Stackable;
        Description = ItemDescription;
    }

    public override void AddItem()
    {
        //Return If Success or not
        bool addItemSuccess = invent.AddItems(this);

        //If Success, Call Use Item
        if (addItemSuccess) UseItem();
    }

    public override void UseItem()
    {
        buffReceiver.Receive(MightBuff);
    }

    public override void DropItem()
    {
        GameObject mightBuffObj = buffReceiver.GetBuffObjWithBuff(MightBuff.GetComponent<Might>());
        mightBuffObj.GetComponent<Might>().RemoveStack();
        base.UseItem();
    }
}
