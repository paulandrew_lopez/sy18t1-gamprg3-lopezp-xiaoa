﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeirdMushroom : ItemBase
{
    const int MAX_PERCENT = 100;
    
    public GameObject Player;
    public int MaxItemStack = 10;
    public int MinItemStack = 1;
    public string ItemDescription = "20% chance to heal self by 5% of max HP, 80% chance to receive 1 stack of Poison for 5s.";

    [Header("Buffs")]
    public GameObject RegenBuff;
    public GameObject PoisonBuff;

    private int positiveEffectRandChance = 20;
    private float RegenMinDuration = 3.0f;
    private BuffReceiver buffReceiver;

    private void Start()
    {
        buffReceiver = Player.GetComponent<BuffReceiver>();

        Name = "Weird Mushroom";
        CurStacks = MinItemStack;
        MaxStacks = MaxItemStack;
        ItemType = TypeOfItem.Both;
        Description = ItemDescription;
    }

    public override void UseItem()
    {
        //Randomize between Regeneration Buff and Poison Buff
        RandomizeEffect();
        base.UseItem();
    }
    private void RandomizeEffect()
    {
        //20 % chance to heal self by 5 % of max HP
        //80 % chance to receive 1 stack of Poison for 5s.
        int rand = Random.Range(0, MAX_PERCENT);

        //0 - 19 == 20 index
        if (rand <= positiveEffectRandChance - 1)
        {
            buffReceiver.Receive(RegenBuff);
        }
        else 
        {
            buffReceiver.Receive(PoisonBuff);
        }
    }
}
