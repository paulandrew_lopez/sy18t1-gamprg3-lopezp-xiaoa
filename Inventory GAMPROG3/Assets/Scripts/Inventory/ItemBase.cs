﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBase : MonoBehaviour
{
    public enum TypeOfItem
    {
        None,
        Stackable,
        Usable,
        Both
    }
    public string Name;
    public int MaxStacks;
    public int CurStacks;
    public TypeOfItem ItemType;
    public Inventory invent;
    public string Description;
   
    public virtual void UseItem()
    {
        invent.UseOrDropItem(this);
    }
    public virtual void AddItem()
    {
        invent.AddItems(this);
    }
    public virtual void DropItem()
    {
        invent.RemoveFromInventory(this);
    }
}


