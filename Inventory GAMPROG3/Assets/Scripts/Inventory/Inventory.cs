﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public int MaxInventorySize = 10;
    public List<ItemBase> InventoryList;


    // add a stack or a new item
    public virtual bool AddItems(ItemBase obj)
    {
        if (MaximumInventorySlots() >= MaxInventorySize)
        {
            Debug.Log("Full Inventory");
            return true;
        }
        if (FindSpecificObject(obj))
        {
            //Obj is stackable 
            bool cond1 = obj.CurStacks < obj.MaxStacks && obj.ItemType == ItemBase.TypeOfItem.Stackable;
            bool cond2 = obj.CurStacks < obj.MaxStacks && obj.ItemType == ItemBase.TypeOfItem.Both;
            if (cond1 || cond2)
            {
                obj.CurStacks++;
                return true;
            }
            else
            {
                return false;
            }
        }
        // Full stack or unstackable or entirely new item
        else
        {
            InventoryList.Add(obj);
            return true;
        }
    }
    //Removes an Instance from the cur stack of an item
    public virtual void UseOrDropItem(ItemBase obj)
    {
        if (obj.CurStacks > 1)
        {
            obj.CurStacks--;
        }
        else
        {
            InventoryList.Remove(obj);
        }
    }
    // Remove an item from inventory
    public virtual void RemoveFromInventory(ItemBase obj)
    {
        InventoryList.Remove(obj);
    }
    // Find and return obj from the list
    public bool FindSpecificObject(ItemBase obj)
    {
        for (int i = 0; i < InventoryList.Count; i++)
        {
            if (InventoryList[i].name == obj.name)
            {
                return true;
            }
        }
        return false;
    }
    public int MaximumInventorySlots()
    {
        int count = 0;
        List<ItemBase> ListOfItems = InventoryList;
        foreach (var item in ListOfItems)
        {
            for (int i = 0; i < item.CurStacks; i++)
            {
                count++;
            }
        }
        return count;
    }
}
