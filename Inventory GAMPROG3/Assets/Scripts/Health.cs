﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    const float HP_PER_VIT = 10.0f;

    public float HP
    {
        get
        {
            return health;
        }
        set
        {
            health = value;

            //Clamping HP
            if (health > MaxHP)
            {
                health = MaxHP;
            }
            else if (health < MinHp) health = MinHp;
        }
    }
    public float MaxHP { get; private set; }
    public float MinHp = 0;

    private float health;
    private Stats stats;
    private float prevVIT;

    //use Awake because other script get HP reference at start
    private void Awake()
    {
        //GetComponent
        stats = GetComponent<Stats>();

        //Initialize
        UpdateHP();
    }

    private void Update()
    {
        //Call only when Vitality has Changed
        if(prevVIT != stats.Vitality)
        {
            UpdateHP();
        }
    }
    void UpdateHP()
    {
        //Store new Vitality
        prevVIT = stats.Vitality;

        //Set HP and MAX HP
        MaxHP = GetDerivedHP();
        HP = MaxHP;
    }
    public bool IsAlive()
    {
        return HP == MinHp;
    }
    /// <summary>
    /// Get Derived HP
    /// </summary>
    /// <returns></returns>
    private float GetDerivedHP()
    {
        return prevVIT * HP_PER_VIT;
    }
}
